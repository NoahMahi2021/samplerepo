
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jnd.advisor.registrationserver.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.grizzly.Grizzly;

/**
 *
 * @author Noah
 */
public class AccessResource {
    
    private static final Logger LOGGER = Grizzly.logger(AccessResource.class);
    
    /* generated keys for ssl */
    private String secureJksFileName = "ssltest-keystore15.jks"; 
    
    /* max size of buffer holding ssl keys */
    final int BUFFER = 4096;
    
    /* name of this class for logging */
    private final String myName = AccessResource.class.getName();    
    /**
     * This method is used in place of URLClassLoader as the underlying code
     * was changed in Java 9+. Otherwise a ClassCastException is thrown when 
     * an attempt is made with this cast: 
     *  URLClassLoader loader =
     *   (URLClassLoader) this.getClass().getClassLoader();
     * @param cl
     * @return byte array holding the resource.
     */
    public byte[] getKeystoreResource(ClassLoader cl) { 
         
        InputStream is = cl.getResourceAsStream(secureJksFileName);
        
        if (is != null) {
            try {
                return readInputStreamContents(is);
            } catch (Exception ex) {
                
                LOGGER.log(Level.WARNING, secureJksFileName + " not found.\n");
                ex.printStackTrace();
            }
        }
        
        return new byte[0];
    }
    
    /**
     * Read the contents of the input stream referencing the resource file.
     * @param contentsIn
     * @return a byte array containing the keystore data.
     * @throws IOException 
     */
    private byte [] readInputStreamContents(InputStream contentsIn) throws IOException {
        
        byte contents[] = new byte[BUFFER];
        int direct;
        BufferedInputStream bif = new BufferedInputStream(contentsIn);

        while ((direct = bif.read(contents, 0, contents.length)) >= 0) {   
            LOGGER.log(Level.INFO,"Resource contains " + direct + 
                " bytes of content.");
        }
        return contents;
    }    
    
}

    

