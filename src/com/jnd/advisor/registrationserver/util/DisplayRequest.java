/**
 * Copyright (c) 2021 jNautics Development LLC and/or its affiliates. 
 * All rights reserved.
 *
 */
package com.jnd.advisor.registrationserver.util;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.grizzly.http.Cookie;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.util.Parameters;

/**
 *
 * @author okios jnautics llc
 */
public class DisplayRequest {
    
    /**
     * Courtesy method to pretty print a server.Request object contents.
     * @param req 
     */
    public String showRequest(Request req) {
        
        StringBuilder sb = new StringBuilder();
        
        Set<String> attributeNames = req.getAttributeNames();
        Set<String> parameterNames = req.getParameterNames();
        Cookie[] cookies = req.getCookies();        
        Iterable<String> headerNames = req.getHeaderNames();
        
        // Another way to try to get at the params but FORM data is not there??
        Parameters params = req.getParameters();
        Set<String> paramNames = params.getParameterNames();
               
        for(String name: headerNames) {
            sb.append("header name: " + name + ",value: " +
                req.getHeader(name) + "\n");
        }
        
        for( String param : paramNames) {
            sb.append("param: " + param + ", val: " + 
                req.getParameter(param) + "\n");            
        }         
                        
        for (String attribute : attributeNames) {
            sb.append("attribute: " + attribute + ", value: " +  
                req.getAttribute(attribute)+ "\n");
        }
        
        for( Cookie cookie : cookies) {
            sb.append("cookie: " + cookie.getName() + ", value: " + 
                cookie.getValue() + "\n");
        }
                
        sb.append("authType: " + req.getAuthType() + "\n");
        sb.append("authorization: " + req.getAuthorization() + "\n");
        sb.append("content length: " + req.getContentLength() + "\n");
        sb.append("contentType: " + req.getContentType() + "\n");
        sb.append("contextPath: " + req.getContextPath() + "\n");       
        sb.append("path info: " + req.getPathInfo() + "\n");
        sb.append("http handler path: " + req.getHttpHandlerPath() + "\n");
        sb.append("local name: " + req.getLocalName() + "\n");
        sb.append("protocol: " + req.getProtocol().getProtocolString() + "\n");
        sb.append("remote address: " + req.getRemoteAddr() + "\n");
        sb.append("remote user: " + req.getRemoteUser() + "\n");
        sb.append("request URL: " + req.getRequestURL().toString() + "\n");
        sb.append("server name: " + req.getServerName() + "\n");
//          sb.append("user principal: " + req.getUserPrincipal().getName() + "\n");
        sb.append("query string: " + req.getQueryString() + "\n"); 
               
        return sb.toString();
        
    }
    

}
