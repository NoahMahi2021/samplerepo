package com.jnd.advisor.registrationserver.http;
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2014 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * Portions Copyright [2021] [jNautics Development LLC]
 *
 * Contributor(s):
 * [jNautics Development LLC] elects to include this software in this 
 * distribution under the [CDDL or GPL Version 2] license.
 * 
 */

// Original package from open source code:
//package org.glassfish.grizzly.samples.httpserver.secure;

import com.jnd.advisor.registrationserver.util.DisplayRequest;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.glassfish.grizzly.http.CookiesBuilder.server;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

/**
 * Simple {@link HttpHandler} implementation.
 */
public class SimpleHttpHandler extends HttpHandler {

    @Override
    public void service(final Request request, final Response response)
            throws Exception {
        
        DisplayRequest dispR = new DisplayRequest();            
        
        Logger.getLogger(SimpleHttpHandler.class.getName())
            .log(Level.INFO, "Service called..." + dispR.showRequest(request));
            
        String descrip = request.getParameter("token") + "\n and...";
        
        if (descrip != null) {
            Logger.getLogger(SimpleHttpHandler.class.getName())
                .log(Level.INFO, descrip); 
        }          
        
//         Send a response with a time stamp and a meaningful messag
       final SimpleDateFormat format = new SimpleDateFormat ("EEE MMM dd, yyyy HH:mm:ss zzz", Locale.US);
    
        final String date = format.format(new Date(System.currentTimeMillis()));
        
          response.setContentType("table.tablesorter.full tr");
         response.getWriter().write("Registration");
   
//         String foo ="<html><head><title>Registration Form</title></head>body></body></html>";
         
//         response.setContentLength(foo.length() + date.length());
         
       response.setContentLength(date.length());
//         
        response.getWriter().write("<html><head><title>Registration Form</title></head>body></body></html>");
        response.getWriter().write(date);

     
       
          response.getWriter().flush();
//        
    // end: service
//    
     }
}