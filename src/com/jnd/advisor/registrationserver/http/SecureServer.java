

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright (c) 2021 jNautics Development LLC and/or its affiliates. 
 * All rights reserved.
 *
 * Copyright (c) 2014-2015 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 * Modifications:
 * Portions Copyright [2021] [jNautics Development LLC]
 *
 * Contributor(s):
 * [jNautics Development LLC] elects to include this software in this 
 * distribution under the [CDDL or GPL Version 2] license.
 */

package com.jnd.advisor.registrationserver.http;
import com.jnd.advisor.registrationserver.util.AccessResource;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.grizzly.Grizzly;
import static org.glassfish.grizzly.http.CookiesBuilder.server;
import org.glassfish.grizzly.http.server.BackendConfiguration;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import static org.glassfish.grizzly.http.util.Header.Server;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;

/**
 * Secured standalone Java HTTP server.
 */
public class SecureServer {
    
     BackendConfiguration bConfig = new BackendConfiguration();
    private static final Logger LOGGER = Grizzly.logger(SecureServer.class);
    
    private static final String keyPassword = "newAccount@1";
 
     private static final String contextPath = "/tmp";
//     private static final String contextPath = "/";
 
    private static final String networkListenerName = "secure-token-listener";
  
   final static HttpServer server = new HttpServer();
   
    public static void main(String[] args) {
//              final HttpServer server = new HttpServer();
        final ServerConfiguration config = server.getServerConfiguration();

        // Register simple HttpHandler
     config.addHttpHandler(new SimpleHttpHandler(), contextPath);
         
            addAnotherHandler();
                
        // For testing other options only...
        // Map the path, /echo, to the NonBlockingEchoHandler
//        config.addHttpHandler(new NonBlockingEchoHandler(), "/echo");
        
//        config.addHttpHandler(new UploaderHttpHandler(), "/echo12");
        
        // create a network listener that listens on port 8080.
        final NetworkListener networkListener = new NetworkListener(
                networkListenerName,
                NetworkListener.DEFAULT_NETWORK_HOST,
                NetworkListener.DEFAULT_NETWORK_PORT);
        
        // Enable SSL on the listener
        networkListener.setSecure(true);
        networkListener.setSSLEngineConfig(createSslConfiguration());
        
        server.addListener(networkListener);        
        
        try {
            // Start the server
            server.start();
            
            LOGGER.log(Level.INFO, "Secured server running & listening with " + 
                networkListener.getName() + " at " + 
                networkListener.getHost() + ":" + networkListener.getPort() +
                "\nPress enter to stop...");
            
// TODO: change this for other shutdown control...            
            // Read any system input
            System.in.read();
            
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
        } finally {
            server.shutdownNow();
        }
    }
    
    /**
     * Initialize server side SSL configuration.
     * 
     * @return server side {@link SSLEngineConfigurator}.
     */
    private static SSLEngineConfigurator createSslConfiguration() {
        // Initialize SSLContext configuration
        SSLContextConfigurator sslContextConfig = new SSLContextConfigurator();

        ClassLoader cl = SecureServer.class.getClassLoader();
        AccessResource ar = new AccessResource();
        
        byte[] keystore = ar.getKeystoreResource(cl);
                
        if (keystore.length > 0) {            
            sslContextConfig.setKeyStoreBytes(keystore);
            sslContextConfig.setKeyStorePass(keyPassword);
            LOGGER.log(Level.INFO, "Keystore file found.");
        } else {
            LOGGER
                .log(Level.WARNING, "keystore file not found!");                        
        }            
            
        // Create SSLEngine configurator
        return new SSLEngineConfigurator(sslContextConfig.createSSLContext(),
                false, false, false);
    }
       
 
   private static void addAnotherHandler(){
  
 String pathToHtmlFiles  =  "C:/Users/Noah/AdivisorServerProject/AnotherSecureServer/src/com/jnd/advisor/registrationserver/index.html";
              
     StaticHttpHandler staticHandler = new StaticHttpHandler(pathToHtmlFiles);
     staticHandler.addDocRoot(contextPath);
      System.out.println("" + staticHandler.getDefaultDocRoot().getAbsolutePath());
 
        server.getServerConfiguration().addHttpHandler(staticHandler);
   }
   
}